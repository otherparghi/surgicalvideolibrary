package com.surgicalvideolibrary.model


class CityListModel{
    var state_id : String =""
    var city_name : String =""
    var state_name : String =""
    var city_id : String =""

    fun setstate_id(state_id : String){
        this.state_id = state_id
    }
    fun setcity_id(city_id : String){
        this.city_id = city_id
    }
    fun setstate_name(state_name : String){
        this.state_name = state_name
    }
    fun setcity_name(city_name : String){
        this.city_name = city_name
    }

    fun getcity_name() : String{
        return city_name
    }

    fun getstate_id() : String{
        return state_id
    }

    fun getcity_id() : String{
        return city_id
    }

    fun getstate_name() : String{
        return state_name
    }
}