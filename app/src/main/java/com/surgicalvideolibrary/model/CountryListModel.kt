package com.surgicalvideolibrary.model

class CountryListModel{

    var country_id : String = ""
    var country_name : String = ""

    fun setcountryid(id : String){
        country_id = id
    }

    fun setcountryname(name : String){
        country_name = name
    }

    fun getcountryid(): String {
        return country_id
    }

    fun getcountryname() : String {
        return country_name
    }
}