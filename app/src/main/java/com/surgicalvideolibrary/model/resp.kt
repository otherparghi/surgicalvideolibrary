package com.example.atul.attendance.model


data class response(val code : String, val message : String, val status : String, val payload : payload)

data class payload(val user : user, val token : String, val countries : ArrayList<country>, val states : ArrayList<state>,
                   var cities : ArrayList<city>, val categories : ArrayList<category>, val videos : ArrayList<videos>,
                   var video : video, val homepage_ad_video : String, var notifications : ArrayList<notifications>)

data class user(val user_id : String, val name : String, val email : String, val phoneno : String, val city : String, val state : String,
                val speciality : String, val country : String, val pincode : String)

data class country(val country_name : String, val country_id : String)

data class state(val state_id : String, val country_name : String, val state_name : String, val country_id : String)

data class city(val state_id : String, val city_name : String, val state_name : String, val city_id : String)

data class category(val category_id : String, val name : String, val video_url : String, val image_url : String)

data class videos(val packagevideo_id : String, val package_id : String, val name : String, val description : String,
                  val video_id : String, val is_new : String, val total_views : String, val category_name : String, val category_id : String,
                  var is_bookmarked : String, val video_image : String)

data class video(val packagevideo_id : String, val name : String, val description : String,
                 val video_id : String, val is_new : String, val total_views : String,
                 val category_name : String, val category_id : String, var is_bookmarked : String,
                 val video_image : String)

data class notifications(var push_notification_id : String, var device_type : String, var device_id : String,
                         var title : String, var message : String, var push_message : String,
                         var is_read : String)

