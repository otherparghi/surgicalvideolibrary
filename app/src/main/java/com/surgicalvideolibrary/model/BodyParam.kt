package com.example.atul.attendance.model

import org.json.JSONArray

data class doAccessBodyParam (var email : String)

data class dogetstateBodyParam (var country_id : String)

data class dogetcityBodyParam (var state_id : String)

data class dosubscriptionupdateBodyParam (var passcode : String)

data class doregisterBodyParam (var email : String, var phoneno : String, var name : String, var speciality : String, var city : String,
                                var state : String, var country : String, var pincode : String)

data class dogethomepagevideoBodyParam(var offset : String, var limit : String, var top_views : String, var total_videos_count : String,
                                      var search_text : String, var exclude_videos : String, var category_id : String)

data class dogetfavouritevideoBodyParam(var offset : String, var limit : String)

data class doaddfavouritevideoBodyParam(var packagevideo_id : String)

data class dogetvideodetailBodyParam(var packagevideo_id : String)

data class docategorywisevideoBodyParam(var category_id : String, var offset : String, var limit : String,
                                        var top_views : String, var total_videos_count : String)