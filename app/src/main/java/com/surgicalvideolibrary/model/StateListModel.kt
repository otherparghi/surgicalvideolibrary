package com.surgicalvideolibrary.model

class StateListModel{
    var state_id : String =""
    var country_name : String =""
    var state_name : String =""
    var country_id : String =""

    fun setstate_id(state_id : String){
        this.state_id = state_id
    }
    fun setcountry_name(country_name : String){
        this.country_name = country_name
    }
    fun setstate_name(state_name : String){
        this.state_name = state_name
    }
    fun setcountry_id(country_id : String){
        this.country_id = country_id
    }

    fun getcountry_id() : String{
        return country_id
    }

    fun getstate_id() : String{
        return state_id
    }

    fun getcountry_name() : String{
        return country_name
    }

    fun getstate_name() : String{
        return state_name
    }
}