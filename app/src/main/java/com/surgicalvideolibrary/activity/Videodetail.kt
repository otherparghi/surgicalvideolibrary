package com.surgicalvideolibrary.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.brightcove.player.edge.Catalog
import com.brightcove.player.edge.VideoListener
import com.brightcove.player.model.Video
import com.example.atul.attendance.model.doaddfavouritevideoBodyParam
import com.example.atul.attendance.model.dogetvideodetailBodyParam
import com.example.atul.attendance.model.response
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.utility.NetworkAvailable
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import kotlinx.android.synthetic.main.activity_videodetail.*
import org.json.JSONObject

class Videodetail : AppCompatActivity(), View.OnClickListener {
    private var stVideoCode = ""
    var packagevideo_id = ""
    var auth_token  = ""
    var mContext = this@Videodetail
    lateinit var internetHandler: Handler
    lateinit var addfavouriteHandler: Handler
    lateinit var viewlogHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_videodetail)

        packagevideo_id = intent.getStringExtra("packagevideo_id")
        auth_token = SharedPreference.GetPreference(mContext, StaticUtility.P_USER_DATA, StaticUtility.TOKEN)!!
        setonclicklistner()

        dogetvideodetail()
        NetworkAvailable(internetHandler).execute()

    }

    //region for setonclicklistner
    private fun setonclicklistner() {
        img_back.setOnClickListener(this)
        img_like.setOnClickListener(this)
    }
    //endregion

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.img_back -> {
                finish()
            }
            R.id.img_like ->{
                doaddfavourite()
                NetworkAvailable(addfavouriteHandler).execute()
            }
        }
    }

    //region for video detail api call
    fun dovideoviewlog(){
        viewlogHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dogetvideodetailBodyParam(packagevideo_id)
                    Apicall.dovideolog(body,viewlogHandler,auth_token, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                            startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {

                        } else {
                            StaticUtility.sendMail(
                                "Getting error in video view log from server in videodetail.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for video detail api call
    fun dogetvideodetail(){
        relative_loader.visibility = View.VISIBLE
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dogetvideodetailBodyParam(packagevideo_id)
                    Apicall.dogetvideodetail(body,internetHandler,auth_token, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                relative_loader.visibility = View.GONE
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                            startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            txt_view.text = response.payload.video.total_views
                            txt_title.text = response.payload.video.name
                            txt_description.text = response.payload.video.description
                            if(response.payload.video.is_bookmarked == "1"){
                                img_like.setImageResource(R.drawable.ic_red_heart)
                            }
                            stVideoCode = response.payload.video.video_id
                            /*Catalog catalog = new Catalog("ZUPNyrUqRdcAtjytsjcJplyUc9ed8b0cD_eWIe36jXqNWKzIcE6i8A..");*/
                            val eventEmitter = brightcove_video_view.eventEmitter
                            val catalog = Catalog(
                                eventEmitter, getString(R.string.account),
                                getString(R.string.policy)
                            )
                            /* catalog.findVideoByID(getString(R.string.videoId), new VideoListener() {*/
                            catalog.findVideoByID(stVideoCode, object : VideoListener() {
                                override fun onVideo(video: Video) {
                                    brightcove_video_view.add(video)
                                    brightcove_video_view.start()
                                    //dovideoviewlog()
                                    //NetworkAvailable(viewlogHandler).execute()
                                }

                                override fun onError(s: String) {
                                    throw RuntimeException(s)
                                }
                            })
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get video detail from server in videodetail.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for add to favourite video api call
    fun doaddfavourite(){
        relative_loader.visibility = View.VISIBLE
        addfavouriteHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = doaddfavouritevideoBodyParam(packagevideo_id)
                    Apicall.doaddfavouritevideo(body,addfavouriteHandler,auth_token, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    relative_loader.visibility = View.GONE
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                            startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            img_like.setImageResource(R.drawable.ic_red_heart)
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in add to favourite from server in videodetail.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion
}
