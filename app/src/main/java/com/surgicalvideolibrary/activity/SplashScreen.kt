package com.surgicalvideolibrary.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import com.example.atul.attendance.model.response
import com.google.firebase.iid.FirebaseInstanceId
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.utility.NetworkAvailable
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import org.json.JSONObject

class SplashScreen : AppCompatActivity() {
    lateinit var internetHandler: Handler
    var mContext = this@SplashScreen
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        StaticUtility.DeviceInfo(this@SplashScreen)
        SharedPreference.CreatePreference(mContext, StaticUtility.VIDEOPREFERENCE)
        SharedPreference.SavePreference(StaticUtility.ISSHOW,"false")
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@SplashScreen) { instanceIdResult ->
            val newToken = instanceIdResult.token
            Log.e("newToken", newToken)
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            //Log.d(TAG, "Refreshed token: " + refreshedToken);
            sharedPreferences.edit().putBoolean(StaticUtility.SENT_TOKEN_TO_SERVER, true).apply()
            sharedPreferences.edit().putString(StaticUtility.FCM_TOKEN, newToken).apply()

            //get token
            //val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            //val newToken = sharedPreferences.getString(StaticUtility.FCM_TOKEN, "")
        }

        dogethomepageadsvideo()
        NetworkAvailable(internetHandler).execute()
        Handler().postDelayed({
            if(SharedPreference.GetPreference(mContext,StaticUtility.P_USER_DATA, StaticUtility.TOKEN) != null){
                startActivity(Intent(mContext, DashboardScreen::class.java))
                finish()
            }else {
                startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                finish()
            }
        },3000)
    }

    //region for get get homepage ads video api call
    fun dogethomepageadsvideo(){
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    Apicall.dogethomepageadsvideo(internetHandler, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                            startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            SharedPreference.CreatePreference(mContext, StaticUtility.VIDEOPREFERENCE)
                            SharedPreference.SavePreference(StaticUtility.HOMEPAGEVIDEO, response.payload.homepage_ad_video)
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get homepage video from server in Splashacreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion
}
