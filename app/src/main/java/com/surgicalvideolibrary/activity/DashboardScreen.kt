package com.surgicalvideolibrary.activity

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.example.atul.attendance.model.category
import com.example.atul.attendance.model.dosubscriptionupdateBodyParam
import com.example.atul.attendance.model.response
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.adapter.CategoryList
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.fragment.Home
import com.surgicalvideolibrary.fragment.Notification
import com.surgicalvideolibrary.fragment.VideoListing
import com.surgicalvideolibrary.utility.NetworkAvailable
import com.surgicalvideolibrary.utility.RecyclerItemClickListener
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import kotlinx.android.synthetic.main.activity_dashboard_screen.*
import kotlinx.android.synthetic.main.activity_video_full_screen.*
import kotlinx.android.synthetic.main.content_dashboard_screen.*
import kotlinx.android.synthetic.main.layout_renew_subscription.view.*
import kotlinx.android.synthetic.main.nav_header_dashboard_screen.view.*
import org.json.JSONObject
import java.lang.ClassCastException

class DashboardScreen : AppCompatActivity(), View.OnClickListener, Home.OnFragmentInteractionListener {
    lateinit var internetHandler: Handler
    var auth_token  = ""
    var user_name  = ""
    var mContext = this@DashboardScreen
    var subscriptioncode = ""
    var category_list : ArrayList<category>? = null
    var doubleBackToExitPressedOnce = false

    var bundle = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_screen)
        auth_token = SharedPreference.GetPreference(mContext,StaticUtility.P_USER_DATA, StaticUtility.TOKEN)!!
        user_name = SharedPreference.GetPreference(mContext,StaticUtility.P_USER_DATA, StaticUtility.USER_NAME)!!

        nav_view.getHeaderView(0).txt_user_name.text = user_name
        //txt_user_name.text = "Dr." + user_name
        setonclicklistner()

        dogetcategory()
        NetworkAvailable(internetHandler).execute()

        StaticUtility.addFragmenttoActivity(supportFragmentManager, Home(), R.id.frame_content)
    }

    //region for setonclicklistner
    private fun setonclicklistner() {
        txtskip.setOnClickListener(this)
        img_menu.setOnClickListener(this)
        txt_renew_subscription.setOnClickListener(this)
        txt_home.setOnClickListener(this)
        txt_notification.setOnClickListener(this)
        txt_top_10_videos.setOnClickListener(this)
        txt_favourite_video.setOnClickListener(this)
        txt_logout.setOnClickListener(this)
        txt_view_by_category.setOnClickListener(this)
        ll_upper_extremity.setOnClickListener(this)
        ll_spine.setOnClickListener(this)
        ll_lower_extremity.setOnClickListener(this)
        ll_sports_medicine.setOnClickListener(this)
        ll_trauma.setOnClickListener(this)
    }
    //endregion


    override fun onFragmentInteraction(bundle: Bundle) {
        /*this.bundle = bundle
        var video_url = SharedPreference.GetPreference(mContext, StaticUtility.VIDEOPREFERENCE, StaticUtility.HOMEPAGEVIDEO)!!
        try {
            spinkit.visibility = View.VISIBLE
            VideoViewBackgroundAsyncTask()
                .execute(video_url)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        videoview.visibility = View.VISIBLE
        ll_main.visibility = View.GONE*/
        StaticUtility.addFragmenttoActivitywithbundle(bundle,supportFragmentManager, VideoListing(), R.id.frame_content)
    }

    //region CLASS FOR SET PROGRESSBAR AND MEDIA-CONTROLLER IN VIDEO-VIEW...
    inner class VideoViewBackgroundAsyncTask : AsyncTask<String, Uri, Void>() {
        override fun onPreExecute() {
        }

        override fun onProgressUpdate(vararg uri: Uri) {
            try {
                //val vidControl = MediaController(mContext)
                //vidControl.setAnchorView(fvwStableVideo)
                //fvwStableVideo.setMediaController(vidControl)
                videoview.setVideoURI(uri[0])
                videoview.requestFocus()
                videoview.setOnPreparedListener {
                    videoview.start()
                    spinkit.visibility = View.GONE

                    Handler().postDelayed({
                        txtskip.visibility = View.VISIBLE
                    },5000)
                }
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            } catch (e: SecurityException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

        }
        override fun doInBackground(vararg params: String): Void? {
            try {
                val uri = Uri.parse(params[0])
                publishProgress(uri)
            } catch (e: Exception) {
                e.printStackTrace()

            }

            return null
        }
    }
    //endregion


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }
            this.doubleBackToExitPressedOnce = true
            StaticUtility.showMessage(mContext,"Please click BACK again to exit")
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.txtskip -> {
                videoview.visibility = View.GONE
                txtskip.visibility = View.GONE
                ll_main.visibility = View.VISIBLE
            }
            R.id.img_menu -> {
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START)
                } else {
                    drawer_layout.openDrawer(GravityCompat.START)
                }
            }
            R.id.txt_renew_subscription -> {
                drawer_layout.closeDrawer(GravityCompat.START)
                subscriptiondialog()
            }
            R.id.txt_home -> {

                StaticUtility.addFragmenttoActivity(supportFragmentManager, Home(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.txt_notification -> {
                StaticUtility.addFragmenttoActivity(supportFragmentManager, Notification(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.txt_top_10_videos -> {
                var bundle = Bundle()
                bundle.putString("category_id","")
                bundle.putString("category_name","topten")
                StaticUtility.addFragmenttoActivitywithbundle(bundle,supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.txt_favourite_video -> {
                var bundle = Bundle()
                bundle.putString("category_id","")
                bundle.putString("category_name","favourite")
                StaticUtility.addFragmenttoActivitywithbundle(bundle,supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.txt_view_by_category -> {
                //StaticUtility.addFragmenttoActivity(supportFragmentManager, VideoListing(), R.id.frame_content)
                //drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.ll_upper_extremity -> {
                StaticUtility.addFragmenttoActivity(supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.ll_spine -> {
                StaticUtility.addFragmenttoActivity(supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.ll_lower_extremity -> {
                StaticUtility.addFragmenttoActivity(supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.ll_sports_medicine -> {
                StaticUtility.addFragmenttoActivity(supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.ll_trauma -> {
                StaticUtility.addFragmenttoActivity(supportFragmentManager, VideoListing(), R.id.frame_content)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.txt_logout -> {
                SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                drawer_layout.closeDrawer(GravityCompat.START)
                startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
            }
        }
    }

    //region for subscription dialog
    private fun subscriptiondialog() {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.layout_renew_subscription, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mDialogView.edt_one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mDialogView.edt_two.requestFocus()
            }
        })
        mDialogView.edt_two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mDialogView.edt_three.requestFocus()
            }
        })
        mDialogView.edt_three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mDialogView.edt_four.requestFocus()
            }
        })
        mDialogView.edt_four.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mDialogView.edt_five.requestFocus()
            }
        })
        mDialogView.edt_five.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mDialogView.edt_six.requestFocus()
            }
        })
        //login button click of custom layout
        mDialogView.btn_submit.setOnClickListener {
            if(!mDialogView.edt_one.text.toString().isEmpty()) {
                subscriptioncode = mDialogView.edt_one.text.toString()
                if(!mDialogView.edt_two.text.toString().isEmpty()) {
                    subscriptioncode += mDialogView.edt_two.text.toString()
                    if(!mDialogView.edt_three.text.toString().isEmpty()) {
                        subscriptioncode += mDialogView.edt_three.text.toString()
                        if(!mDialogView.edt_four.text.toString().isEmpty()) {
                            subscriptioncode += mDialogView.edt_four.text.toString()
                            if(!mDialogView.edt_five.text.toString().isEmpty()) {
                                subscriptioncode += mDialogView.edt_five.text.toString()
                                if(!mDialogView.edt_six.text.toString().isEmpty()) {
                                    subscriptioncode += mDialogView.edt_six.text.toString()
                                    dosubscribe(mAlertDialog)
                                    NetworkAvailable(internetHandler).execute()
                                }else{
                                    mDialogView.edt_six.requestFocus()
                                    StaticUtility.showMessage(mContext,"Enter passcode...!")
                                }
                            }else{
                                mDialogView.edt_five.requestFocus()
                                StaticUtility.showMessage(mContext,"Enter passcode...!")
                            }
                        }else{
                            mDialogView.edt_four.requestFocus()
                            StaticUtility.showMessage(mContext,"Enter passcode...!")
                        }
                    }else{
                        mDialogView.edt_three.requestFocus()
                        StaticUtility.showMessage(mContext,"Enter passcode...!")
                    }
                }else{
                    mDialogView.edt_two.requestFocus()
                    StaticUtility.showMessage(mContext,"Enter passcode...!")
                }
            }else{
                mDialogView.edt_one.requestFocus()
                StaticUtility.showMessage(mContext,"Enter passcode...!")
            }
        }

    }
    //endregion

    //region for sunscription update api call
    fun dosubscribe(alertDialog: AlertDialog){
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dosubscriptionupdateBodyParam(subscriptioncode)
                    Apicall.dosubscribe(body,internetHandler, auth_token, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                            startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            alertDialog.dismiss()
                            StaticUtility.showMessage(mContext,response.message)
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getcity from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get category api call
    fun dogetcategory(){
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    Apicall.dogetcategory(internetHandler, auth_token, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(mContext, StaticUtility.P_USER_DATA)
                            startActivity(Intent(mContext, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            category_list = response.payload.categories
                            recycler_view_category.layoutManager = LinearLayoutManager(mContext)
                            recycler_view_category.adapter = CategoryList(mContext, category_list!!)
                            recycler_view_category.addOnItemTouchListener(RecyclerItemClickListener(mContext,recycler_view_category,
                                object : RecyclerItemClickListener.OnItemClickListener{
                                    override fun onItemClick(view: View, position: Int) {
                                        var bundle : Bundle = Bundle()
                                        bundle.putString("category_id",category_list!![position].category_id)
                                        bundle.putString("category_name",category_list!![position].name)
                                        StaticUtility.addFragmenttoActivitywithbundle(bundle,supportFragmentManager, VideoListing(),
                                            R.id.frame_content)
                                        drawer_layout.closeDrawer(GravityCompat.START)
                                    }

                                    override fun onItemLongClick(view: View?, position: Int) {

                                    }

                                }))
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getcity from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion
}
