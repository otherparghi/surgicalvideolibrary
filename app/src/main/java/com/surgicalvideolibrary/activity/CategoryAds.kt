package com.surgicalvideolibrary.activity

import android.media.MediaPlayer
import android.net.Uri
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import kotlinx.android.synthetic.main.activity_video_full_screen.*



class CategoryAds : AppCompatActivity(), View.OnClickListener {
    var video_url = ""
    var mContext = this@CategoryAds
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_ads)
        video_url = intent.getStringExtra("video_url")

        if(video_url == ""){
            finish()
        }
        setonclicklistner()
        try {
            spin_kit.visibility = View.VISIBLE
            VideoViewBackgroundAsyncTask()
                .execute(video_url)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.txt_skip->{
                finish()
            }
        }
    }

    //region for set onclick listner
    private fun setonclicklistner() {
        txt_skip.setOnClickListener(this)
    }
    //endregion

    //region CLASS FOR SET PROGRESSBAR AND MEDIA-CONTROLLER IN VIDEO-VIEW...
    inner class VideoViewBackgroundAsyncTask : AsyncTask<String, Uri, Void>() {
        override fun onPreExecute() {
        }

        override fun onProgressUpdate(vararg uri: Uri) {
            try {
                //val vidControl = MediaController(mContext)
                //vidControl.setAnchorView(fvwStableVideo)
                //fvwStableVideo.setMediaController(vidControl)
                fvwStableVideo.setVideoURI(uri[0])
                fvwStableVideo.requestFocus()
                fvwStableVideo.setOnPreparedListener {
                    fvwStableVideo.start()
                    spin_kit.visibility = View.GONE

                    Handler().postDelayed({
                        txt_skip.visibility = View.VISIBLE
                    },5000)
                }
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            } catch (e: SecurityException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

        }

        override fun doInBackground(vararg params: String): Void? {
            try {
                val uri = Uri.parse(params[0])
                publishProgress(uri)
            } catch (e: Exception) {
                e.printStackTrace()

            }

            return null
        }
    }
    //endregion

    override fun onBackPressed() {
        //super.onBackPressed()
    }
}
