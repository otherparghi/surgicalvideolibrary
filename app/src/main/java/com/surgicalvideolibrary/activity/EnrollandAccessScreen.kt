package com.surgicalvideolibrary.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import com.example.atul.attendance.model.*
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.adapter.CityList
import com.surgicalvideolibrary.adapter.CountryList
import com.surgicalvideolibrary.adapter.StateList
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.model.CityListModel
import com.surgicalvideolibrary.model.CountryListModel
import com.surgicalvideolibrary.model.StateListModel
import com.surgicalvideolibrary.utility.NetworkAvailable
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import kotlinx.android.synthetic.main.activity_enrolland_access_screen.*
import org.json.JSONObject
import java.lang.ClassCastException

class EnrollandAccessScreen : AppCompatActivity(), View.OnClickListener {
    lateinit var internetHandler: Handler
    private var mContext = this@EnrollandAccessScreen
    var countrylist : ArrayList<CountryListModel> = ArrayList()
    var statelist : ArrayList<StateListModel> = ArrayList()
    var citylist : ArrayList<CityListModel> = ArrayList()
    var country_id : String = ""
    var state_id : String = ""
    var city_id : String = ""
    var country_name : String = ""
    var state_name : String = ""
    var city_name : String = ""

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_submit -> {
                if(valid()) {
                    doRegister()
                    NetworkAvailable(internetHandler).execute()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enrolland_access_screen)

        setonclicklistner()


        edit_email.setOnEditorActionListener { _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                if(!edit_email.text.isNullOrEmpty()) {
                    if(android.util.Patterns.EMAIL_ADDRESS.matcher(edit_email.text.toString()).matches()){
                        StaticUtility.hideSoftKeyBoard(mContext,edit_email)
                        doAccess()
                        NetworkAvailable(internetHandler).execute()
                    }else{
                        edit_email.requestFocus()
                        edit_email.error = "Enter valid email id...!"
                    }
                }
                true
            } else {
                false
            }
        }

        setupcountrystatecity()

        dogetCountry()
        NetworkAvailable(internetHandler).execute()
    }

    //region For setup country state and city list
    private fun setupcountrystatecity() {
        var countrylistmodel = CountryListModel()
        countrylistmodel.setcountryname("Select Country")
        countrylistmodel.setcountryid("-1")
        countrylist.add(countrylistmodel)

        var statelistmdel = StateListModel()
        statelistmdel.setcountry_id("-1")
        statelistmdel.setcountry_name("Select Country")
        statelistmdel.setstate_id("-1")
        statelistmdel.setstate_name("Select State")
        statelist.add(statelistmdel)

        var citylistmodel = CityListModel()
        citylistmodel.setcity_id("-1")
        citylistmodel.setcity_name("Select City")
        citylistmodel.setstate_id("-1")
        citylistmodel.setstate_name("Select State")
        citylist.add(citylistmodel)

        setdatacountry()
        setdatastate()
        setdatacity()
    }
    //endregion

    //region for set data in city spinner
    private fun setdatacity() {
        sp_city.adapter = CityList(mContext, citylist)
        if(city_name != ""){
            var i = 0
            for(city in citylist){
                if(city_name.toLowerCase() == city.city_name.toLowerCase()){
                    break
                }
                i++
            }
            sp_city.setSelection(i)
        }
        sp_city.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //offer_id = offerlist?.get(position)?.offer_id ?: "0"
                city_name = citylist[position].getcity_name()
                city_id = citylist[position].getcity_id()
            }
        }
    }
    //endregion

    //region for set data in state spinner
    private fun setdatastate() {
        sp_state.adapter = StateList(mContext, statelist)
        if(state_name != ""){
            var i = 0
            for(state in statelist){
                if(state_name.toLowerCase() == state.state_name.toLowerCase()){
                    break
                }
                i++
            }
            sp_state.setSelection(i)
        }
        sp_state.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                state_id = statelist[position].getstate_id()
                state_name = statelist[position].getstate_name()
                if(state_id != "-1"){
                    dogetCity()
                    NetworkAvailable(internetHandler).execute()
                }
            }

        }
    }
    //endregion

    //region for set data in country spinner
    private fun setdatacountry() {
        sp_country.adapter = CountryList(mContext, countrylist)
        if(country_name != "") {
            var i = 0
            for (country in countrylist) {
                if (country_name.toLowerCase() == country.country_name.toLowerCase()) {
                    break
                }
                i++
            }
            sp_country.setSelection(i)
        }
        sp_country.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                country_id = countrylist[position].getcountryid()
                country_name = countrylist[position].getcountryname()
                if(country_id != "-1") {
                    dogetState()
                    NetworkAvailable(internetHandler).execute()
                }

            }

        }
    }
    //endregion

    //region for setonclick listner
    private fun setonclicklistner() {
        btn_submit.setOnClickListener(this)
    }
    //endregion

    //region for aceess api call
    fun doAccess(){
        relative_loader.visibility = View.VISIBLE
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    val body = doAccessBodyParam(edit_email.text.toString())
                    Apicall.doAccess(body, internetHandler, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            var user = response.payload.user
                            if(!user.user_id.isNullOrEmpty()){
                                edit_mobile_no.setText(user.phoneno)
                                edit_name.setText(user.name)
                                edit_speciality.setText(user.speciality)
                                edit_pincode.setText(user.pincode)
                                country_name = user.country
                                state_name = user.state
                                city_name = user.city
                                setdatacountry()
                            }else{

                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getuserdata from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
                relative_loader.visibility = View.GONE
            }
            true
        })
    }
    //endregion

    //region for validation
    private fun valid(): Boolean {
        if(!edit_mobile_no.text.toString().isEmpty()){
            if(android.util.Patterns.PHONE.matcher(edit_mobile_no.text.toString()).matches()){
                if(!edit_name.text.toString().isEmpty()){
                    if(!edit_speciality.text.toString().isEmpty()){
                        if(country_id != "-1"){
                            if(state_id != "-1"){
                                if(city_id != "-1"){
                                    if(!edit_pincode.text.toString().isEmpty()) {
                                        return true
                                    }else{
                                        edit_pincode.requestFocus()
                                        edit_pincode.error = "Enter Pincode...!"
                                    }
                                }else{
                                    StaticUtility.showMessage(mContext,"Select City...!")
                                }
                            }else{
                                StaticUtility.showMessage(mContext,"Select State...!")
                            }
                        }else{
                            StaticUtility.showMessage(mContext,"Select Country...!")
                        }
                    }else{
                        edit_speciality.requestFocus()
                        edit_speciality.error = "Enter speciality...!"
                    }
                }else{
                    edit_name.requestFocus()
                    edit_name.error = "Enter name...!"
                }
            }else{
                edit_mobile_no.requestFocus()
                edit_mobile_no.error = "Enter valid mobile no...!"
            }
        }else{
            edit_mobile_no.requestFocus()
            edit_mobile_no.error = "Enter mobile no...!"
        }
        return false
    }
    //endregion

    //region for get country api call
    fun dogetCountry(){
        relative_loader.visibility = View.VISIBLE
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    Apicall.dogetcountry(internetHandler, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    relative_loader.visibility = View.GONE
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            var countries = response.payload.countries
                            if(countries.size > 0) {
                                for (country in countries) {
                                    var countrylistmodel = CountryListModel()
                                    countrylistmodel.setcountryid(country.country_id)
                                    countrylistmodel.setcountryname(country.country_name)
                                    countrylist.add(countrylistmodel)
                                }
                                setdatacountry()
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getcountry from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get state api call
    fun dogetState(){
        relative_loader.visibility = View.VISIBLE
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dogetstateBodyParam(country_id)
                    Apicall.dogetstate(body,internetHandler, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    relative_loader.visibility = View.GONE
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            var states = response.payload.states
                            if(states.size > 0) {
                                for (state in states) {
                                    var stateListModel = StateListModel()
                                    stateListModel.setstate_id(state.state_id)
                                    stateListModel.setstate_name(state.state_name)
                                    stateListModel.setcountry_id(state.country_id)
                                    stateListModel.setcountry_name(state.country_name)
                                    statelist.add(stateListModel)

                                }
                                setdatastate()
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getstate from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get city api call
    fun dogetCity(){
        relative_loader.visibility = View.VISIBLE
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dogetcityBodyParam(state_id)
                    Apicall.dogetcity(body,internetHandler, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    relative_loader.visibility = View.GONE
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            var cities = response.payload.cities
                            if(cities.size > 0) {
                                for (city in cities) {
                                    var citylistmodel = CityListModel()
                                    citylistmodel.setstate_id(city.state_id)
                                    citylistmodel.setstate_name(city.state_name)
                                    citylistmodel.setcity_id(city.city_id)
                                    citylistmodel.setcity_name(city.city_name)
                                    citylist.add(citylistmodel)

                                }
                                setdatacity()
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getcity from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for register api call
    fun doRegister(){
        relative_loader.visibility = View.VISIBLE
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = doregisterBodyParam(edit_email.text.toString(),edit_mobile_no.text.toString(), edit_name.text.toString(),
                        edit_speciality.text.toString(), city_name, state_name, country_name, edit_pincode.text.toString())
                    Apicall.doregister(body,internetHandler, mContext)
                }else
                    StaticUtility.showMessage(mContext,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    relative_loader.visibility = View.GONE
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        StaticUtility.showMessage(mContext, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            StaticUtility.showMessage(mContext, response.message)
                            SharedPreference.CreatePreference(mContext, StaticUtility.P_USER_DATA)
                            SharedPreference.SavePreference(StaticUtility.TOKEN,response.payload.token)
                            SharedPreference.SavePreference(StaticUtility.USER_NAME,edit_name.text.toString())
                            startActivity(Intent(this@EnrollandAccessScreen, DashboardScreen::class.java))
                            finish()
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getcity from server in EnrollandAccessScreen.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion
}
