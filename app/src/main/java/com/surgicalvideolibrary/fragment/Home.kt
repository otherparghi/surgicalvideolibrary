package com.surgicalvideolibrary.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.atul.attendance.model.*

import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.activity.EnrollandAccessScreen
import com.surgicalvideolibrary.activity.HomeAds
import com.surgicalvideolibrary.activity.Videodetail
import com.surgicalvideolibrary.adapter.FavouriteVideoList
import com.surgicalvideolibrary.adapter.RecentlyUplodedVideoList
import com.surgicalvideolibrary.adapter.TopTenVideosList
import com.surgicalvideolibrary.adapter.ViewByCategory
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.utility.NetworkAvailable
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONObject


class Home : Fragment() {
    lateinit var internetHandler: Handler
    lateinit var topTenHandler: Handler
    lateinit var favouriteHandler: Handler
    lateinit var addfavouriteHandler: Handler
    lateinit var viewbycategoryHandler: Handler

    var offset = 0
    var limit = 20

    var offsetfavourite = 0
    var limitfavourite = 20
    var auth_token : String = ""

    var isrecentuploadvieo = true
    var isrecentuploadvieosetup = true
    var istoptenvieosetup = true
    var isfavouritevieosetup = true
    var isfavouritevieo = true

    var recentlyuploadvideo_list : ArrayList<videos> = ArrayList()
    var toptenvideo_list : ArrayList<videos> = ArrayList()
    var favouritevideo_list : ArrayList<videos> = ArrayList()
    var categories_list : ArrayList<category> = ArrayList()

    private var listener: OnFragmentInteractionListener? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_home, container, false)
        auth_token = SharedPreference.GetPreference(activity!!, StaticUtility.P_USER_DATA, StaticUtility.TOKEN)!!
        if(SharedPreference.GetPreference(activity!!, StaticUtility.VIDEOPREFERENCE, StaticUtility.ISSHOW) != null &&
            SharedPreference.GetPreference(activity!!, StaticUtility.VIDEOPREFERENCE, StaticUtility.ISSHOW).equals("false")) {
            startActivity(Intent(activity!!, HomeAds::class.java))
        }
        dogetrecentlyuploadedvideo(isrecentuploadvieosetup,view)
        NetworkAvailable(internetHandler).execute()

        dogetviewbycategory(view)
        NetworkAvailable(viewbycategoryHandler).execute()

        dogetoptenvideo(istoptenvieosetup,view)
        NetworkAvailable(topTenHandler).execute()

        dogetfavouritevideo(isfavouritevieosetup,view)
        NetworkAvailable(favouriteHandler).execute()

        return view
    }

    //region for get view by category api call
    fun dogetviewbycategory(view : View){

            view.relative_loader.visibility = View.VISIBLE


        viewbycategoryHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    Apicall.dogetviewbycategory(viewbycategoryHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {

                    view.relative_loader.visibility = View.GONE

                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                                    txt_view_by_category.visibility = View.VISIBLE
                            recycler_view_view_by_category_list.visibility = View.VISIBLE
                                    categories_list = response.payload.categories
                                    recycler_view_view_by_category_list.layoutManager = LinearLayoutManager(
                                        activity!!,
                                        LinearLayoutManager.HORIZONTAL, false
                                    )
                                    recycler_view_view_by_category_list.adapter =
                                        ViewByCategory(activity!!,
                                            categories_list, object : ViewByCategory.OnClick {
                                                override fun Onclick(category: category) {

                                                    var bundle : Bundle = Bundle()
                                                    bundle.putString("category_id",category.category_id)
                                                    bundle.putString("category_name",category.name)
                                                    bundle.putString("video_url",category.video_url)
                                                    listener!!.onFragmentInteraction(bundle)
                                                }
                                            })


                        } else {
                            StaticUtility.sendMail(
                                "Getting error in view by category from server in home.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get recently uploded video api call
    fun dogetrecentlyuploadedvideo(issetup : Boolean, view : View){
        if(issetup) {
            view.relative_loader.visibility = View.VISIBLE
        }

        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dogethomepagevideoBodyParam(offset.toString(),limit.toString(),0.toString(),1.toString(),
                        "","","")
                    Apicall.dogethomepagevideo(body,internetHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if(issetup) {
                        view.relative_loader.visibility = View.GONE
                    }
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            if(response.payload.videos.size > 0) {
                                offset += limit
                                if (issetup) {
                                    txt_recent_upload_video.visibility = View.VISIBLE
                                    recycler_view_recently_uploaded_videos_list.visibility = View.VISIBLE
                                    recentlyuploadvideo_list = response.payload.videos
                                    recycler_view_recently_uploaded_videos_list.layoutManager = LinearLayoutManager(
                                        activity!!,
                                        LinearLayoutManager.HORIZONTAL, false
                                    )
                                    recycler_view_recently_uploaded_videos_list.adapter =
                                        RecentlyUplodedVideoList(activity!!,
                                            recentlyuploadvideo_list, object : RecentlyUplodedVideoList.OnClick {
                                                override fun Onclickvideo(videos: videos) {

                                                   /* startActivity(
                                                        Intent(activity!!, VideoFullScreen::class.java).putExtra("packagevideo_id",
                                                            videos.packagevideo_id))*/

                                                    startActivity(
                                                        Intent(activity!!, Videodetail::class.java).putExtra("packagevideo_id",
                                                            videos.packagevideo_id))
                                                }

                                                override fun Onclick(videos: videos) {
                                                    doaddfavourite(videos.packagevideo_id)
                                                    NetworkAvailable(addfavouriteHandler).execute()
                                                }

                                            })
                                    recycler_view_recently_uploaded_videos_list.addOnScrollListener(object :
                                        RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            super.onScrolled(recyclerView, dx, dy)
                                            val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                                            //position starts at 0
                                            val lastVisibleItemPosition = layoutManager!!.findLastVisibleItemPosition()
                                            if (lastVisibleItemPosition >= layoutManager.itemCount - 5) {
                                                if (isrecentuploadvieo) {
                                                    isrecentuploadvieosetup = false
                                                    dogetrecentlyuploadedvideo(isrecentuploadvieosetup,getView()!!)
                                                    NetworkAvailable(internetHandler).execute()
                                                }
                                            }
                                        }
                                    })
                                } else {
                                    recentlyuploadvideo_list.addAll(response.payload.videos)
                                    recycler_view_recently_uploaded_videos_list.adapter!!.notifyDataSetChanged()
                                }
                            }else{
                                isrecentuploadvieo = false
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getrecently uplodedvideo from server in home.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get topten video api call
    fun dogetoptenvideo(issetup : Boolean,view: View){
        if(issetup) {
            view.relative_loader.visibility = View.VISIBLE
        }
        topTenHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                   /* var body = dogethomepagevideo(offset.toString(),limit.toString(),0.toString(),1.toString(),
                        "","")*/
                    var body = dogethomepagevideoBodyParam(0.toString(),10.toString(),1.toString(),1.toString(),
                        "","","")
                    Apicall.dogettoptenvideo(body,topTenHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if(issetup) {
                        view.relative_loader.visibility = View.GONE
                    }
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                                if (issetup) {
                                    txt_top_ten.visibility = View.VISIBLE
                                    recycler_view_top_10_videos_list.visibility = View.VISIBLE
                                    //offset += limit
                                    toptenvideo_list = response.payload.videos
                                    recycler_view_top_10_videos_list.layoutManager = LinearLayoutManager(
                                        activity!!,
                                        LinearLayoutManager.HORIZONTAL, false
                                    )
                                    recycler_view_top_10_videos_list.adapter = TopTenVideosList(activity!!,
                                        toptenvideo_list, object : TopTenVideosList.OnClick {
                                            override fun OnclickVideo(videos: videos) {
                                                startActivity(
                                                    Intent(activity!!, Videodetail::class.java).putExtra("packagevideo_id",
                                                        videos.packagevideo_id))
                                            }

                                            override fun Onclick(videos: videos) {
                                                doaddfavourite(videos.packagevideo_id)
                                                NetworkAvailable(addfavouriteHandler).execute()
                                            }

                                        })
                                    recycler_view_top_10_videos_list.addOnScrollListener(object :
                                        RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            super.onScrolled(recyclerView, dx, dy)
                                            val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                                            //position starts at 0
                                            val lastVisibleItemPosition = layoutManager!!.findLastVisibleItemPosition()
                                            if (lastVisibleItemPosition >= layoutManager.itemCount - 5) {
                                                /*if (isrecentuploadvieo) {
                                                isrecentuploadvieosetup = false
                                                isrecentuploadvieo = false
                                                dogetrecentlyuploadedvideo(isrecentuploadvieosetup)
                                                NetworkAvailable(internetHandler).execute()
                                            }*/
                                            }
                                        }
                                    })
                                } else {
                                    toptenvideo_list.addAll(response.payload.videos)
                                    recycler_view_top_10_videos_list.adapter!!.notifyDataSetChanged()
                                }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get top ten video from server in home.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get favourite video api call
    fun dogetfavouritevideo(issetup : Boolean,view: View){
        if(issetup) {
            view.relative_loader.visibility = View.VISIBLE
        }
        favouriteHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    /* var body = dogethomepagevideo(offset.toString(),limit.toString(),0.toString(),1.toString(),
                         "","")*/
                    var body = dogetfavouritevideoBodyParam(offsetfavourite.toString(), limitfavourite.toString())
                    Apicall.dogetfavouritevideo(body,favouriteHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                if(issetup) {
                    view.relative_loader.visibility = View.GONE
                }
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            if(response.payload.videos.size > 0) {
                                if(response.payload.videos.size < limitfavourite){
                                    isfavouritevieo = false
                                }
                                offsetfavourite += limitfavourite
                                if (issetup) {
                                    txt_favourite_video.visibility = View.VISIBLE
                                    recycler_view_favourite_video_list.visibility = View.VISIBLE
                                    favouritevideo_list = response.payload.videos
                                    recycler_view_favourite_video_list.layoutManager = LinearLayoutManager(
                                        activity!!,
                                        LinearLayoutManager.HORIZONTAL, false
                                    )
                                    recycler_view_favourite_video_list.adapter = FavouriteVideoList(activity!!,
                                        favouritevideo_list, object : FavouriteVideoList.OnClick {
                                            override fun OnclickVideo(videos: videos) {
                                                startActivity(
                                                    Intent(activity!!, Videodetail::class.java).putExtra("packagevideo_id",
                                                        videos.packagevideo_id))
                                            }

                                            override fun Onclick(videos: videos) {
                                                doaddfavourite(videos.packagevideo_id)
                                                NetworkAvailable(addfavouriteHandler).execute()
                                            }

                                        })
                                    recycler_view_favourite_video_list.addOnScrollListener(object :
                                        RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            super.onScrolled(recyclerView, dx, dy)
                                            val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                                            //position starts at 0
                                            val lastVisibleItemPosition = layoutManager!!.findLastVisibleItemPosition()
                                            if (lastVisibleItemPosition >= layoutManager.itemCount - 5) {
                                                if (isfavouritevieo) {
                                                    isfavouritevieosetup = false
                                                    dogetfavouritevideo(isfavouritevieosetup,getView()!!)
                                                    NetworkAvailable(favouriteHandler).execute()
                                                }
                                            }
                                        }
                                    })
                                } else {
                                    if(response.payload.videos.size < limitfavourite){
                                        isfavouritevieo = false
                                    }
                                    favouritevideo_list.addAll(response.payload.videos)
                                    recycler_view_favourite_video_list.adapter!!.notifyDataSetChanged()
                                }
                            }else{
                                isfavouritevieo = false
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get favourite video from server in home.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for add to favourite video api call
    fun doaddfavourite(packagevideo_id  : String){
        addfavouriteHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = doaddfavouritevideoBodyParam(packagevideo_id)
                    Apicall.doaddfavouritevideo(body,addfavouriteHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            offsetfavourite = 0
                            limitfavourite = 20
                            isfavouritevieosetup = true
                            dogetfavouritevideo(isfavouritevieosetup,view!!)
                            NetworkAvailable(favouriteHandler).execute()
                            var i = 0
                            for(video in recentlyuploadvideo_list) {
                                if(packagevideo_id == video.packagevideo_id){
                                    if(recentlyuploadvideo_list[i].is_bookmarked == "1") {
                                        recentlyuploadvideo_list[i].is_bookmarked = "0"
                                    }else{
                                        recentlyuploadvideo_list[i].is_bookmarked = "1"
                                    }
                                }
                                i++
                            }
                            i = 0
                            for(video in toptenvideo_list) {
                                if(packagevideo_id == video.packagevideo_id){
                                    if(toptenvideo_list[i].is_bookmarked == "1") {
                                        toptenvideo_list[i].is_bookmarked = "0"
                                    }else{
                                        toptenvideo_list[i].is_bookmarked = "1"
                                    }
                                }
                                i++
                            }
                            recycler_view_recently_uploaded_videos_list.adapter!!.notifyDataSetChanged()
                            recycler_view_top_10_videos_list.adapter!!.notifyDataSetChanged()
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in add to favourite from server in home.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(bundle: Bundle)
    }
}
