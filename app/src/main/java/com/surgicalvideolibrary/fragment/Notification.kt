package com.surgicalvideolibrary.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.atul.attendance.model.notifications
import com.example.atul.attendance.model.response

import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.activity.EnrollandAccessScreen
import com.surgicalvideolibrary.adapter.NotificationList
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.utility.*
import kotlinx.android.synthetic.main.fragment_notification.*
import org.json.JSONObject


class Notification : Fragment() {
    lateinit var internetHandler: Handler
    var notification_list : ArrayList<notifications> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_notification, container, false)

        dogetnotification()
        NetworkAvailable(internetHandler).execute()

        return view
    }

    //region for get get notification list api call
    fun dogetnotification(){
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    Apicall.dogetnotification(internetHandler, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            if(response.payload.notifications != null){
                                notification_list = response.payload.notifications
                                recycler_notification_list.layoutManager = LinearLayoutManager(activity!!)
                                recycler_notification_list.adapter = NotificationList(activity!!,
                                    notification_list, object : NotificationList.OnClick{
                                    override fun Onclick(notifications: notifications) {

                                    }
                                })

                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get notification list from server in notification.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion
}
