package com.surgicalvideolibrary.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.atul.attendance.model.*

import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.activity.CategoryAds
import com.surgicalvideolibrary.activity.EnrollandAccessScreen
import com.surgicalvideolibrary.activity.Videodetail
import com.surgicalvideolibrary.adapter.CategoryWiseVideoList
import com.surgicalvideolibrary.api.Apicall
import com.surgicalvideolibrary.utility.NetworkAvailable
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import kotlinx.android.synthetic.main.fragment_video_listing.*
import kotlinx.android.synthetic.main.fragment_video_listing.view.*
import org.json.JSONObject

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class VideoListing : Fragment() {

    var offset = 0
    var limit = 20

    var offsetfavourite = 0
    var limitfavourite = 20
    var auth_token : String = ""
    lateinit var internetHandler: Handler
    lateinit var addfavouriteHandler: Handler
    lateinit var favouriteHandler: Handler
    lateinit var topTenHandler: Handler
    var category_id = ""
    var category_name = ""
    var video_url = ""
    var issetup = true
    var isloadmore= true

    var isfavouritevieosetup = true
    var isfavouritevieo = true

    var favouritevideo_list : ArrayList<videos> = ArrayList()
    var toptenvideo_list : ArrayList<videos> = ArrayList()

    var video_list : ArrayList<videos> = ArrayList()

    var issearchtopten = false
    var issearchfavourite = false
    var issearchcategory = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_video_listing, container, false)

        if(arguments != null && arguments!!.containsKey("category_id")){
            category_id = arguments!!.getString("category_id")
        }

        if(arguments != null && arguments!!.containsKey("category_name")){
            category_name = arguments!!.getString("category_name")
        }

        if(arguments != null && arguments!!.containsKey("video_url")){
            video_url = arguments!!.getString("video_url")
        }

        startActivity(Intent(activity!!, CategoryAds::class.java).putExtra("video_url",video_url))
        auth_token = SharedPreference.GetPreference(activity!!, StaticUtility.P_USER_DATA, StaticUtility.TOKEN)!!
        if(category_name == "topten"){
            issearchtopten = true
            view.txt_title.text = "Top 10 Videos"
            dogetoptenvideo(true,view,"")
            NetworkAvailable(topTenHandler).execute()
        }else if(category_name == "favourite"){
            issearchfavourite = true
            view.txt_title.text = "Favourite Videos"
            dogetfavouritevideo(isfavouritevieosetup,view)
            NetworkAvailable(favouriteHandler).execute()
        }else {
            issearchcategory = true
            view.txt_title.text = category_name
            dogetvideolist(issetup,view,"")
            NetworkAvailable(internetHandler).execute()
        }

        view.edt_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                dofilter(p0)
            }
        })

        return view
    }

    private fun dofilter(p0: CharSequence?) {
        when {
            issearchcategory -> {
                offset = 0
                dogetvideolist(true,view!!,p0.toString())
                NetworkAvailable(internetHandler).execute()
            }
            issearchfavourite -> {

            }
            issearchtopten -> {
                dogetoptenvideo(true,view!!,p0.toString())
                NetworkAvailable(topTenHandler).execute()
            }
        }
    }

    //region for get topten video api call
    fun dogetoptenvideo(issetup : Boolean, view: View, search : String){
        if(issetup){
            view.relative_loader.visibility = View.VISIBLE
        }
        topTenHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    /* var body = dogethomepagevideo(offset.toString(),limit.toString(),0.toString(),1.toString(),
                         "","")*/
                    var body = dogethomepagevideoBodyParam(0.toString(),10.toString(),1.toString(),1.toString(),
                        search,"","")
                    Apicall.dogettoptenvideo(body,topTenHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if(issetup){
                        view.relative_loader.visibility = View.GONE
                    }
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            if (issetup) {
                                //offset += limit
                                toptenvideo_list = response.payload.videos
                                recycler_video_list.layoutManager = GridLayoutManager(activity!!, 2)
                                recycler_video_list.adapter = CategoryWiseVideoList(activity!!,
                                    toptenvideo_list, object : CategoryWiseVideoList.OnClick {
                                        override fun Onclickvideo(videos: videos) {
                                            startActivity(Intent(activity!!,Videodetail::class.java).putExtra("packagevideo_id",
                                                videos.packagevideo_id))
                                        }
                                        override fun Onclick(videos: videos) {
                                            doaddfavourite(videos.packagevideo_id,getView()!!)
                                            NetworkAvailable(addfavouriteHandler).execute()
                                        }
                                    })
                                recycler_video_list.addOnScrollListener(object :
                                    RecyclerView.OnScrollListener() {
                                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        super.onScrolled(recyclerView, dx, dy)
                                        val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                                        //position starts at 0
                                        val lastVisibleItemPosition = layoutManager!!.findLastVisibleItemPosition()
                                        if (lastVisibleItemPosition >= layoutManager.itemCount - 5) {
                                            if (isfavouritevieo) {
                                                isfavouritevieosetup = false
                                                dogetfavouritevideo(isfavouritevieosetup, getView()!!)
                                                NetworkAvailable(favouriteHandler).execute()
                                            }
                                        }
                                    }
                                })
                            } else {
                                toptenvideo_list.addAll(response.payload.videos)
                                recycler_video_list.adapter!!.notifyDataSetChanged()
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get top ten video from server in home.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get favourite video api call
    fun dogetfavouritevideo(issetup : Boolean, view: View){
        if(issetup){
            view.relative_loader.visibility = View.VISIBLE
        }
        favouriteHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    /* var body = dogethomepagevideo(offset.toString(),limit.toString(),0.toString(),1.toString(),
                         "","")*/
                    var body = dogetfavouritevideoBodyParam(offsetfavourite.toString(), limitfavourite.toString())
                    Apicall.dogetfavouritevideo(body,favouriteHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if(issetup){
                        view.relative_loader.visibility = View.GONE
                    }
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            if(response.payload.videos.size > 0) {
                                if(response.payload.videos.size < limitfavourite){
                                    isfavouritevieo = false
                                }
                                offsetfavourite += limitfavourite
                                if (issetup) {
                                    favouritevideo_list = response.payload.videos
                                    for((i, _) in favouritevideo_list.withIndex()) {
                                        favouritevideo_list[i].is_bookmarked = "1"
                                    }
                                    recycler_video_list.layoutManager = GridLayoutManager(activity!!, 2)
                                    recycler_video_list.adapter = CategoryWiseVideoList(activity!!,
                                        favouritevideo_list, object : CategoryWiseVideoList.OnClick {
                                            override fun Onclickvideo(videos: videos) {
                                                startActivity(Intent(activity!!,Videodetail::class.java).putExtra("packagevideo_id",
                                                    videos.packagevideo_id))
                                            }
                                            override fun Onclick(videos: videos) {
                                                doaddfavourite(videos.packagevideo_id,getView()!!)
                                                NetworkAvailable(addfavouriteHandler).execute()
                                            }
                                        })
                                    recycler_video_list.addOnScrollListener(object :
                                        RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            super.onScrolled(recyclerView, dx, dy)
                                            val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                                            //position starts at 0
                                            val lastVisibleItemPosition = layoutManager!!.findLastVisibleItemPosition()
                                            if (lastVisibleItemPosition >= layoutManager.itemCount - 5) {
                                                if (isfavouritevieo) {
                                                    isfavouritevieosetup = false
                                                    dogetfavouritevideo(isfavouritevieosetup, getView()!!)
                                                    NetworkAvailable(favouriteHandler).execute()
                                                }
                                            }
                                        }
                                    })
                                } else {
                                    if(response.payload.videos.size < limitfavourite){
                                        isfavouritevieo = false
                                    }
                                    favouritevideo_list.addAll(response.payload.videos)
                                    recycler_video_list.adapter!!.notifyDataSetChanged()
                                }
                            }else{
                                isfavouritevieo = false
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in getfavourite from server in videolisting.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for get video list api call
    fun dogetvideolist(issetup : Boolean, view: View, search : String){
        if(issetup){
            view.relative_loader.visibility = View.VISIBLE
        }
        internetHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = dogethomepagevideoBodyParam(offset.toString(),limit.toString(),0.toString(),1.toString(),
                        search,"",category_id)
                    Apicall.dogetcategorywisevideo(body,internetHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                try {
                    if(issetup){
                        view.relative_loader.visibility = View.GONE
                    }
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            if(response.payload.videos.size > 0) {
                                if (issetup) {
                                    offset += limit
                                    video_list = response.payload.videos
                                    recycler_video_list.layoutManager = GridLayoutManager(activity!!, 2)
                                    recycler_video_list.adapter = CategoryWiseVideoList(activity!!,
                                        video_list, object : CategoryWiseVideoList.OnClick {
                                            override fun Onclickvideo(videos: videos) {
                                                startActivity(Intent(activity!!,Videodetail::class.java).putExtra("packagevideo_id",
                                                    videos.packagevideo_id))
                                            }

                                            override fun Onclick(videos: videos) {
                                                doaddfavourite(videos.packagevideo_id,getView()!!)
                                                NetworkAvailable(addfavouriteHandler).execute()
                                            }

                                        })
                                    recycler_video_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            super.onScrolled(recyclerView, dx, dy)
                                            val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
                                            //position starts at 0
                                            val lastVisibleItemPosition = layoutManager!!.findLastVisibleItemPosition()
                                            if (lastVisibleItemPosition >= layoutManager.itemCount - 5) {
                                                if (isloadmore) {
                                                    dogetvideolist(false,getView()!!,"")
                                                    NetworkAvailable(internetHandler).execute()
                                                }
                                            }
                                        }
                                    })
                                } else {
                                    video_list.addAll(response.payload.videos)
                                    recycler_video_list.adapter!!.notifyDataSetChanged()
                                }
                            }else{
                                isloadmore = false
                            }
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in get videolist from server in videolist.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

    //region for add to favourite video api call
    fun doaddfavourite(packagevideo_id  : String, view: View){
        view.relative_loader.visibility = View.VISIBLE
        addfavouriteHandler = Handler(Handler.Callback { msg ->
            if(msg.arg1 == 1){
                if(msg.obj as Boolean) {
                    var body = doaddfavouritevideoBodyParam(packagevideo_id)
                    Apicall.doaddfavouritevideo(body,addfavouriteHandler,auth_token, activity!!)
                }else
                    StaticUtility.showMessage(activity!!,getString(R.string.network_error))
            }else if(msg.arg1 == 0){
                view.relative_loader.visibility = View.GONE
                try {
                    if (msg.arg2 == 0) {
                        val jsonObject = JSONObject(msg.obj.toString())
                        if(jsonObject.optString("code") == "401"){
                            SharedPreference.ClearPreference(activity!!, StaticUtility.P_USER_DATA)
                            startActivity(Intent(activity!!, EnrollandAccessScreen::class.java))
                        }
                        StaticUtility.showMessage(activity!!, jsonObject.optString("message"))
                    } else {
                        val response = msg.obj as response
                        if (response.code == "200") {
                            for((i, video) in video_list.withIndex()) {
                                if(packagevideo_id == video.packagevideo_id){
                                    if(video_list[i].is_bookmarked == "1") {
                                        video_list[i].is_bookmarked = "0"
                                    }else{
                                        video_list[i].is_bookmarked = "1"
                                    }
                                }
                            }
                            recycler_video_list.adapter!!.notifyDataSetChanged()
                        } else {
                            StaticUtility.sendMail(
                                "Getting error in add to favourite from server in videolist.\n",
                                response.toString()
                            )
                        }
                    }
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }
            }
            true
        })
    }
    //endregion

}
