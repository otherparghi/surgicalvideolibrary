package com.example.atul.retrofit.api

import com.example.atul.attendance.model.*
import com.surgicalvideolibrary.utility.StaticUtility
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface ApiInterface {

    @POST(StaticUtility.ACCESS)
    fun apicall(@Header("Content-Type") Content_Type : String,
                @Header("app-id") App_Id : String,
                @Header("app-secret") app_secret : String,
                @QueryMap(encoded = true) query: Map<String, String>,
                @Body body : doAccessBodyParam): Call<response>

    @POST(StaticUtility.GETCOUNTRY)
    fun getcountry(@Header("Content-Type") Content_Type : String,
                @Header("app-id") App_Id : String,
                @Header("app-secret") app_secret : String,
                   @QueryMap(encoded = true) query: Map<String, String>): Call<response>

    @POST(StaticUtility.GETSTATE)
    fun getstate(@Header("Content-Type") Content_Type : String,
                   @Header("app-id") App_Id : String,
                   @Header("app-secret") app_secret : String,
                 @QueryMap(encoded = true) query: Map<String, String>,
                 @Body body: dogetstateBodyParam): Call<response>

    @POST(StaticUtility.GETCITY)
    fun getcity(@Header("Content-Type") Content_Type : String,
                 @Header("app-id") App_Id : String,
                 @Header("app-secret") app_secret : String,
                @QueryMap(encoded = true) query: Map<String, String>,
                 @Body body: dogetcityBodyParam): Call<response>

    @POST(StaticUtility.REGISTER)
    fun register(@Header("Content-Type") Content_Type : String,
                @Header("app-id") App_Id : String,
                @Header("app-secret") app_secret : String,
                 @QueryMap(encoded = true) query: Map<String, String>,
                @Body body: doregisterBodyParam): Call<response>

    @POST(StaticUtility.SUBSCRIPTION)
    fun dosubscription(@Header("Content-Type") Content_Type : String,
                 @Header("app-id") App_Id : String,
                 @Header("app-secret") app_secret : String,
                 @Header("auth-token") auth_token : String,
                       @QueryMap(encoded = true) query: Map<String, String>,
                 @Body body: dosubscriptionupdateBodyParam): Call<response>

    @POST(StaticUtility.GETCATEGORY)
    fun dogetcategory(@Header("Content-Type") Content_Type : String,
                       @Header("app-id") App_Id : String,
                       @Header("app-secret") app_secret : String,
                       @Header("auth-token") auth_token : String,
                      @QueryMap(encoded = true) query: Map<String, String>): Call<response>

    @POST(StaticUtility.GETHOMEPAGEVIDEO)
    fun dogethomepagevideo(@Header("Content-Type") Content_Type : String,
                      @Header("app-id") App_Id : String,
                      @Header("app-secret") app_secret : String,
                      @Header("auth-token") auth_token : String,
                           @QueryMap(encoded = true) query: Map<String, String>,
                           @Body body : dogethomepagevideoBodyParam): Call<response>

    @POST(StaticUtility.GETFAVOURITEVIDEO)
    fun dogetfavouritevideo(@Header("Content-Type") Content_Type : String,
                           @Header("app-id") App_Id : String,
                           @Header("app-secret") app_secret : String,
                           @Header("auth-token") auth_token : String,
                            @QueryMap(encoded = true) query: Map<String, String>,
                           @Body body : dogetfavouritevideoBodyParam): Call<response>

    @POST(StaticUtility.ADDFAVOURITEVIDEO)
    fun doaddfavouritevideo(@Header("Content-Type") Content_Type : String,
                            @Header("app-id") App_Id : String,
                            @Header("app-secret") app_secret : String,
                            @Header("auth-token") auth_token : String,
                            @QueryMap(encoded = true) query: Map<String, String>,
                            @Body body : doaddfavouritevideoBodyParam): Call<response>

    @POST(StaticUtility.GETHOMEPAGEVIDEO)
    fun dogetcategorywisevideo(@Header("Content-Type") Content_Type : String,
                            @Header("app-id") App_Id : String,
                            @Header("app-secret") app_secret : String,
                            @Header("auth-token") auth_token : String,
                               @QueryMap(encoded = true) query: Map<String, String>,
                            @Body body : dogethomepagevideoBodyParam): Call<response>

    @POST(StaticUtility.GETVIDEODETAIL)
    fun dogetvideodetail(@Header("Content-Type") Content_Type : String,
                               @Header("app-id") App_Id : String,
                               @Header("app-secret") app_secret : String,
                               @Header("auth-token") auth_token : String,
                         @QueryMap(encoded = true) query: Map<String, String>,
                               @Body body : dogetvideodetailBodyParam): Call<response>

    @POST(StaticUtility.VIDEOVIEWLOG)
    fun videolog(@Header("Content-Type") Content_Type : String,
                         @Header("app-id") App_Id : String,
                         @Header("app-secret") app_secret : String,
                         @Header("auth-token") auth_token : String,
                         @QueryMap(encoded = true) query: Map<String, String>,
                         @Body body : dogetvideodetailBodyParam): Call<response>

    @POST(StaticUtility.HOMEPAGEADSVIDEO)
    fun gethomepagevideo(@Header("Content-Type") Content_Type : String,
                         @Header("app-id") App_Id : String,
                         @Header("app-secret") app_secret : String,
                         @QueryMap(encoded = true) query: Map<String, String>): Call<response>

    @POST(StaticUtility.GETVIEWBYCATEGORY)
    fun getviewbycategory(@Header("Content-Type") Content_Type : String,
                         @Header("app-id") App_Id : String,
                         @Header("app-secret") app_secret : String,
                         @Header("auth-token") auth_token : String,
                          @QueryMap(encoded = true) query: Map<String, String>): Call<response>

    @POST(StaticUtility.GETNOTIFICATIONLIST)
    fun getnotificationlist(@Header("Content-Type") Content_Type : String,
                          @Header("app-id") App_Id : String,
                          @Header("app-secret") app_secret : String,
                          @Header("auth-token") auth_token : String,
                          @QueryMap(encoded = true) query: Map<String, String>): Call<response>
}