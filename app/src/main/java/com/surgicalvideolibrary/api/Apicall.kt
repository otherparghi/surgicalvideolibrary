package com.surgicalvideolibrary.api

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import com.example.atul.attendance.model.*
import com.example.atul.retrofit.api.ApiClient
import com.example.atul.retrofit.api.ApiInterface
import com.surgicalvideolibrary.utility.SharedPreference
import com.surgicalvideolibrary.utility.StaticUtility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object Apicall {

    //region for api call
    fun doAccess(body : doAccessBodyParam, handler : Handler, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.apicall(
                StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,
            StaticUtility.queryStringUrl(context), body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                        "Getting error in getuserdata from server in EnrollandAccessScreen.\n",
                        t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get country list
    fun dogetcountry(handler : Handler, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.getcountry(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,StaticUtility.queryStringUrl(context)
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in getcountry list from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get state list
    fun dogetstate(body : dogetstateBodyParam,handler : Handler, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.getstate(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in getstate list from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion


    //region for api call get city list
    fun dogetcity(body : dogetcityBodyParam,handler : Handler, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.getcity(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in getcity list from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call register
    fun doregister(body : doregisterBodyParam,handler : Handler, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.register(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in register from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get city list
    fun dosubscribe(body : dosubscriptionupdateBodyParam,handler : Handler, auth_token : String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dosubscription(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token, StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in subscribe list from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get city list
    fun dogetcategory(handler : Handler, auth_token : String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dogetcategory(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,
            StaticUtility.queryStringUrl(context)
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get category list from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get home page video
    fun dogethomepagevideo(body : dogethomepagevideoBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dogethomepagevideo(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get recently uploaded video from server in home.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get top ten video
    fun dogettoptenvideo(body : dogethomepagevideoBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dogethomepagevideo(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get top ten video from server in home.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get favourite video
    fun dogetfavouritevideo(body : dogetfavouritevideoBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dogetfavouritevideo(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get favourite video from server in home.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call add favourite video
    fun doaddfavouritevideo(body : doaddfavouritevideoBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.doaddfavouritevideo(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in add favourite video from server in home.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get category wise video
    fun dogetcategorywisevideo(body : dogethomepagevideoBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dogetcategorywisevideo(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get category wise from server in videolisting.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get category wise video
    fun dogetvideodetail(body : dogetvideodetailBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.dogetvideodetail(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get videodetail from server in videodetail.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get category wise video
    fun dovideolog(body : dogetvideodetailBodyParam,handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.videolog(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token,StaticUtility.queryStringUrl(context),
            body
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in videoview log server in video detail.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get city list
    fun dogethomepageadsvideo(handler : Handler, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.gethomepagevideo(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,StaticUtility.queryStringUrl(context)
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get homepage ads video list from server in EnrollandAccessScreen.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get home page video
    fun dogetviewbycategory(handler : Handler, auth_token  :String, context: Context) {
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.getviewbycategory(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET, auth_token, StaticUtility.queryStringUrl(context)
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get view by category from server in home.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion

    //region for api call get notification list
    fun dogetnotification(handler : Handler, context: Context) {
        var auth_token = SharedPreference.GetPreference(context, StaticUtility.P_USER_DATA, StaticUtility.TOKEN)!!
        val client = ApiClient.client.create(ApiInterface::class.java)
        client.getnotificationlist(
            StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,auth_token,StaticUtility.queryStringUrl(context)
        ).enqueue(object : Callback<response> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<response>, response: Response<response>) {
                val msg = Message()
                if(response.code() == 200) {
                    msg.obj = response.body()
                    msg.arg2 = 1
                }else{
                    msg.obj = StaticUtility.convertStreamToString(response).replace("\n","")
                    msg.arg2 = 0
                }
                msg.arg1 = 0
                handler.sendMessage(msg)
            }

            override fun onFailure(call: Call<response>, t: Throwable) {
                //StaticUtility.showMessage(mContext, t.toString())
                //Creating SendMail object
                StaticUtility.sendMail(
                    "Getting error in get notification list from server in Notification.\n",
                    t.toString()
                )
            }
        })
    }
    //endregion
}