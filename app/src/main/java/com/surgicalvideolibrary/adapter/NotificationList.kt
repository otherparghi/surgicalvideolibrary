package com.surgicalvideolibrary.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.example.atul.attendance.model.notifications
import com.surgicalvideolibrary.R
import kotlinx.android.synthetic.main.row_layout_notification_list.view.*

class NotificationList(val context: Context, val notification_list : ArrayList<notifications>,
                     onItemClickListener: OnClick) : RecyclerView.Adapter<NotificationList.notificationHolder>() {

    var onclick : OnClick? = null
    var onItemClickListener = onItemClickListener
    // Gets the number of contacts in the list
    override fun getItemCount(): Int {
        return notification_list.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): notificationHolder {
        return notificationHolder(LayoutInflater.from(context).inflate(R.layout.row_layout_notification_list, parent, false))
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(notificationolder: notificationHolder, position: Int) {
        //holder?.tvAnimalType?.text = items.get(position)
        notificationolder.txt_message.text = notification_list[position].message
    }

    inner class notificationHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val txt_message = view.txt_message



    }

    interface OnClick{
        fun Onclick(notifications: notifications)
    }
}