package com.surgicalvideolibrary.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.example.atul.attendance.model.videos
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.utility.GlideApp
import kotlinx.android.synthetic.main.row_layout_category_wise_video_list.view.*

class CategoryWiseVideoList(val context: Context, val video_list : ArrayList<videos>,
                            onItemClickListener: OnClick) : RecyclerView.Adapter<CategoryWiseVideoList.categorywisevideolistHolder>() {

    var onclick : OnClick? = null
    var onItemClickListener = onItemClickListener
    // Gets the number of contacts in the list
    override fun getItemCount(): Int {
        return video_list.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): categorywisevideolistHolder {
        return categorywisevideolistHolder(LayoutInflater.from(context).inflate(R.layout.row_layout_category_wise_video_list, parent, false))
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(categorywisevideolistholder: categorywisevideolistHolder, position: Int) {
        //holder?.tvAnimalType?.text = items.get(position)

        if(video_list[position].is_bookmarked == "1"){
            categorywisevideolistholder.img_book_mark.setImageResource(R.drawable.ic_red_heart)
        }else{
            categorywisevideolistholder.img_book_mark.setImageResource(R.drawable.ic_heart)
        }
        val params = LinearLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT)
        if(position % 2 != 0){
            params.setMargins(20,30,0,0)
        }else{
            params.setMargins(0,30,0,0)
        }
        categorywisevideolistholder.relative_main.layoutParams = params
        if(video_list[position].is_new == "1"){
            categorywisevideolistholder.txt_is_new.visibility = View.VISIBLE
        }else{
            categorywisevideolistholder.txt_is_new.visibility = View.GONE
        }
        GlideApp.with(context)
            .load(video_list[position].video_image)
            //.placeholder(R.drawable.placeholder)
            //.error(R.drawable.imagenotfound)
            .fitCenter()
            .into(categorywisevideolistholder.img_view_thumb)
        //recentlyuplodedvideoholder.txt_desc.text = video_list[position].description
        categorywisevideolistholder.txt_view.text = video_list[position].total_views

        categorywisevideolistholder.img_book_mark.setOnClickListener{
            onclick = onItemClickListener
            onclick?.Onclick(video_list[position])
        }

        categorywisevideolistholder.relative_main.setOnClickListener{
            onclick = onItemClickListener
            onclick?.Onclickvideo(video_list[position])
        }


    }

    inner class categorywisevideolistHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val img_view_thumb = view.img_view_thumb
        val txt_desc = view.txt_desc
        val img_view = view.img_view
        val txt_view = view.txt_view
        val img_book_mark = view.img_book_mark
        val relative_main = view.relative_main
        val ll_main = view.ll_main
        val txt_is_new = view.txt_is_new

    }

    interface OnClick{
        fun Onclick(videos: videos)
        fun Onclickvideo(videos: videos)
    }
}

