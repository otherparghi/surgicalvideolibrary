package com.surgicalvideolibrary.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.example.atul.attendance.model.category
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.utility.GlideApp
import kotlinx.android.synthetic.main.row_layout_view_by_category.view.*

class ViewByCategory(val context: Context, val category_list : ArrayList<category>,
                     onItemClickListener: OnClick) : RecyclerView.Adapter<ViewByCategory.viewbycategoryHolder>() {

    var onclick : OnClick? = null
    var onItemClickListener = onItemClickListener
    // Gets the number of contacts in the list
    override fun getItemCount(): Int {
        return category_list.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewbycategoryHolder {
        return viewbycategoryHolder(LayoutInflater.from(context).inflate(R.layout.row_layout_view_by_category, parent, false))
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(viewbycategoryholder: viewbycategoryHolder, position: Int) {
        //holder?.tvAnimalType?.text = items.get(position)
        val params = LinearLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT)
        when (position) {
            category_list.lastIndex -> params.setMargins(40,0,60,0)
            0 -> params.setMargins(60,0,0,0)
            else -> params.setMargins(40,0,0,0)
        }
        viewbycategoryholder.relative_main.layoutParams = params
        viewbycategoryholder.txt_category_name.text = category_list[position].name

        if(category_list[position].image_url != null && category_list[position].image_url != "") {
            GlideApp.with(context)
                .load(category_list[position].image_url)
                .placeholder(R.drawable.img_placeholder)
                //.error(R.drawable.imagenotfound)
                .fitCenter()
                .into(viewbycategoryholder.img_category)
        }
        viewbycategoryholder.relative_main.setOnClickListener{
            onclick = onItemClickListener
            onclick?.Onclick(category_list[position])
        }
    }

    inner class viewbycategoryHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val img_category = view.img_category
        val txt_category_name = view.txt_category_name
        val relative_main = view.relative_main



    }

    interface OnClick{
        fun Onclick(category: category)
    }
}