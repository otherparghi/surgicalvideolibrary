package com.surgicalvideolibrary.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.model.CountryListModel

class CountryList(context: Context, country_list : ArrayList<CountryListModel>) : BaseAdapter() {
    var context = context
    var country_list = country_list
    private val mInflator: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return country_list.size
    }

    override fun getItem(position: Int): Any {
        return country_list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val vh: CountryListHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.row_layout_list_country_spinner, parent, false)
            vh = CountryListHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as CountryListHolder
        }
        if(position != 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                vh.txt_country_name.setTextColor(context.getColor(R.color.black))
            }else{
                vh.txt_country_name.setTextColor(context.resources.getColor(R.color.black))
            }
        }
        vh.txt_country_name.text = country_list[position].getcountryname()
        return view
    }
}

class CountryListHolder(row: View?) {
    val txt_country_name: TextView = row?.findViewById(R.id.txt_country_name) as TextView

}