package com.surgicalvideolibrary.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.example.atul.attendance.model.videos
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.utility.GlideApp
import kotlinx.android.synthetic.main.row_layout_video_list.view.*

class RecentlyUplodedVideoList(val context: Context, val video_list : ArrayList<videos>,
                               onItemClickListener: OnClick) : RecyclerView.Adapter<RecentlyUplodedVideoList.recentlyuplodedvideolistHolder>() {

    var onclick : OnClick? = null
    var onItemClickListener = onItemClickListener
    // Gets the number of contacts in the list
    override fun getItemCount(): Int {
        return video_list.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): recentlyuplodedvideolistHolder {
        return recentlyuplodedvideolistHolder(LayoutInflater.from(context).inflate(R.layout.row_layout_video_list, parent, false))
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(recentlyuplodedvideoholder: recentlyuplodedvideolistHolder, position: Int) {
        //holder?.tvAnimalType?.text = items.get(position)

        if(video_list[position].is_bookmarked == "1"){
            recentlyuplodedvideoholder.img_book_mark.setImageResource(R.drawable.ic_red_heart)
        }else{
            recentlyuplodedvideoholder.img_book_mark.setImageResource(R.drawable.ic_heart)
        }
        val params = LinearLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT)
        when (position) {
            video_list.lastIndex -> params.setMargins(40,0,60,0)
            0 -> params.setMargins(60,0,0,0)
            else -> params.setMargins(40,0,0,0)
        }
        recentlyuplodedvideoholder.relative_main.layoutParams = params
        GlideApp.with(context)
            .load(video_list[position].video_image)
            //.placeholder(R.drawable.placeholder)
            //.error(R.drawable.imagenotfound)
            .fitCenter()
            .into(recentlyuplodedvideoholder.img_view_thumb)
        recentlyuplodedvideoholder.txt_desc.text = Html.fromHtml(video_list[position].description)
        recentlyuplodedvideoholder.txt_view.text = video_list[position].total_views

        recentlyuplodedvideoholder.img_book_mark.setOnClickListener{
            onclick = onItemClickListener
            onclick?.Onclick(video_list[position])
        }

        recentlyuplodedvideoholder.relative_main.setOnClickListener{
            onclick = onItemClickListener
            onclick?.Onclickvideo(video_list[position])
        }


    }

    inner class recentlyuplodedvideolistHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val img_view_thumb = view.img_view_thumb
        val txt_desc = view.txt_desc
        val img_view = view.img_view
        val txt_view = view.txt_view
        val img_book_mark = view.img_book_mark
        val relative_main = view.relative_main
        val ll_main = view.ll_main



    }

    interface OnClick{
        fun Onclick(videos: videos)
        fun Onclickvideo(videos: videos)
    }
}

