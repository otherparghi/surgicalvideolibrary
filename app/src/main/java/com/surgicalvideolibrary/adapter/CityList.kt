package com.surgicalvideolibrary.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.model.CityListModel

class CityList(context: Context, city_list : ArrayList<CityListModel>) : BaseAdapter() {
    var context = context
    var city_list = city_list
    private val mInflator: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return city_list.size
    }

    override fun getItem(position: Int): Any {
        return city_list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val vh: CityListHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.row_layout_list_city_spinner, parent, false)
            vh = CityListHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as CityListHolder
        }
        if(position != 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                vh.txt_city_name.setTextColor(context.getColor(R.color.black))
            }else{
                vh.txt_city_name.setTextColor(context.resources.getColor(R.color.black))
            }
        }
        vh.txt_city_name.text = city_list[position].getcity_name()
        return view
    }
}

class CityListHolder(row: View?) {
    val txt_city_name: TextView = row?.findViewById(R.id.txt_city_name) as TextView

}