package com.surgicalvideolibrary.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.atul.attendance.model.category
import com.surgicalvideolibrary.R
import kotlinx.android.synthetic.main.row_layout_category.view.*

class CategoryList(val context: Context, val category_list : ArrayList<category>) : RecyclerView.Adapter<CategoryList.categorylistHolder>() {
    var onItemClick: ((category) -> Unit)? = null

    // Gets the number of contacts in the list
    override fun getItemCount(): Int {
        return category_list.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): categorylistHolder {
        return categorylistHolder(LayoutInflater.from(context).inflate(R.layout.row_layout_category, parent, false))
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(categorylistholder: categorylistHolder, position: Int) {
        //holder?.tvAnimalType?.text = items.get(position)
        categorylistholder.txt_category_name.text = category_list[position].name
    }

    inner class categorylistHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val txt_category_name = view.txt_category_name
        val ll_upper_extremity = view.ll_upper_extremity

        init {
            view.setOnClickListener {
                onItemClick?.invoke(category_list[adapterPosition])
            }
        }

    }
}

