package com.surgicalvideolibrary.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.surgicalvideolibrary.R
import com.surgicalvideolibrary.model.StateListModel

class StateList(context: Context, state_list : ArrayList<StateListModel>) : BaseAdapter() {
    var context = context
    var state_list = state_list
    private val mInflator: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return state_list.size
    }

    override fun getItem(position: Int): Any {
        return state_list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val vh: StateListHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.row_layout_list_state_spinner, parent, false)
            vh = StateListHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as StateListHolder
        }
        if(position != 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                vh.txt_state_name.setTextColor(context.getColor(R.color.black))
            }else{
                vh.txt_state_name.setTextColor(context.resources.getColor(R.color.black))
            }
        }
        vh.txt_state_name.text = state_list[position].getstate_name()
        return view
    }
}

class StateListHolder(row: View?) {
    val txt_state_name: TextView = row?.findViewById(R.id.txt_state_name) as TextView

}