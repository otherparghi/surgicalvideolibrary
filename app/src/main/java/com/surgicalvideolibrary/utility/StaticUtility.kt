package com.surgicalvideolibrary.utility

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.atul.attendance.model.response
import retrofit2.Response
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader


object StaticUtility {

    val APP_ID = "2b037c2b580f6e9f4fc01trt02dcebc4549"
    val APP_SECRET = "e7ae15388f07d4c1260rtr6d0c2fac25670"
    val CONTENT_TYPE = "application/json"


    //val URL = "http://192.168.0.107/crm/attendence_api/"
    val URL = "http://surgicalvideolibrary.com/"

    //region For send mail to developer for any exception in app
    val EMAIL = "tt957621@gmail.com"
    val PASSWORD = "tt957621@gmail.com1"
    val TOEMAIL = "atul@parghiinfotech.com"
    val SUBJECT = "SurgicalVideos  app exception Report"
    //endregion

    //region For SharedPreferences
    val P_USER_DATA = "user_data"
    val TOKEN = "token"
    val USER_NAME = "user_name"
    //endregion

    //region For api list
    const val ACCESS = "mob/auth/get-by-email"
    const val GETCOUNTRY = "mob/auth/get-countries"
    const val GETSTATE = "mob/auth/get-states"
    const val GETCITY = "mob/auth/get-cities"
    const val REGISTER = "mob/auth/register"
    const val SUBSCRIPTION = "mob/api/upgrade-subscription"
    const val GETCATEGORY = "mob/categories/list"
    const val GETHOMEPAGEVIDEO = "mob/api/all-videos"
    const val GETFAVOURITEVIDEO = "mob/api/bookmarked-videos-list"
    const val ADDFAVOURITEVIDEO = "mob/api/video-bookmarked"
    const val GETCATEGORYWISEVIDEO = "mob/api/categories/single"
    const val GETVIDEODETAIL = "mob/api/single-video-detail"
    const val HOMEPAGEADSVIDEO = "mob/home-page-video"
    const val GETVIEWBYCATEGORY = "mob/categories/list"
    const val GETNOTIFICATIONLIST = "mob/api/getnotifications"
    const val VIDEOVIEWLOG = "mob/api/video-viewed"
    //endregion

    //region For Fcm
    //notification legacy key = AIzaSyDTFRk7RX-vcn-wJn-VOSv7ujSOcBR5LcM
    val SENT_TOKEN_TO_SERVER = "sentTokenToServer"
    val FCM_TOKEN = "fcm_token"
    val PUSH_NOTIFICATION = "pushNotification"
    //endregion

    //region For Preferences Name
    val LOGINPREFERENCE = "loginpreference"
    val VIDEOPREFERENCE = "videopreference"
    //endregion

    //region For Preferences Key
    val USER_ID = "user_id"
    val HOMEPAGEVIDEO = "homepage_video"
    val ISSHOW = "is_show"
    //endregion

    //region For device information
    var devicename = ""
    var deviceos = ""
    var app_version = ""
    var device_id = ""
    val DEVICEINFOPREFERENCE = "deviceinfoPreference"
    var DeviceName = "DeviceName"
    var DeviceOs = "DeviceOs"
    var App_Version = "App_Version"
    //endregion

    //region for send email to developer
    fun sendMail(message : String, response : String){
        val sm = SendMail(StaticUtility.TOEMAIL, StaticUtility.SUBJECT,
            message+response,
            StaticUtility.EMAIL, StaticUtility.PASSWORD)
        //Executing sendmail to send email
        sm.execute()
    }
    //endregion

    //region for show message
    fun showMessage(context : Context, message : String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
    //endregion


    //region For add fragment
    fun addFragmenttoActivity(manager: FragmentManager, fragment: Fragment, frameId: Int) {

        val transaction = manager.beginTransaction()
        transaction.add(frameId, fragment)
        transaction.commit()

    }
    //endregion

    //region For add fragment with bundle
    fun addFragmenttoActivitywithbundle(bundle : Bundle, manager: FragmentManager, fragment: Fragment, frameId: Int) {

        fragment.arguments = bundle
        val transaction = manager.beginTransaction()
        transaction.add(frameId, fragment)
        transaction.commit()

    }
    //endregion

    //region DeviceInfo
    fun DeviceInfo(mContext: Context) {
        val model = Build.MODEL
        val os = Build.VERSION.RELEASE
        val manager = mContext.packageManager
        var info: PackageInfo? = null
        try {
            info = manager.getPackageInfo(
                mContext.packageName, 0
            )
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            ///Creating SendMail object
            StaticUtility.sendMail(
                "Getting error in Get device information to server in MainActivity.\n",
                e.toString()
            )
        }

        assert(info != null)
        val version = info!!.versionName
        SharedPreference.CreatePreference(mContext, StaticUtility.DEVICEINFOPREFERENCE)
        SharedPreference.SavePreference(StaticUtility.DeviceName, model)
        SharedPreference.SavePreference(StaticUtility.DeviceOs, os)
        SharedPreference.SavePreference(StaticUtility.App_Version, version)
    }//endregion

    //region For make static url...
    fun queryStringUrl(context: Context): HashMap<String, String> {
        val query = HashMap<String, String>()
        try {
            devicename =
                SharedPreference.GetPreference(context, StaticUtility.DEVICEINFOPREFERENCE, StaticUtility.DeviceName).toString()
            deviceos =
                SharedPreference.GetPreference(context, StaticUtility.DEVICEINFOPREFERENCE, StaticUtility.DeviceOs).toString()
            app_version =
                SharedPreference.GetPreference(context, StaticUtility.DEVICEINFOPREFERENCE, StaticUtility.App_Version).toString()
            device_id =
                SharedPreference.GetPreference(context, StaticUtility.SENT_TOKEN_TO_SERVER, StaticUtility.FCM_TOKEN).toString()
            devicename = devicename.replace(" ", "")
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        query["app_type"] = "Android"
        query["app_version"] = app_version
        query["device_name"] = devicename
        query["system_version"] = deviceos
        query["device_id"] = device_id
        /*return "?app_type=Android&app_version=" + app_version + "&device_name=" + devicename + "&system_version=" +
                deviceos + "&device_id=" + device_id*/
        return query

    }
    //endregion

    //region for convert response to string
    fun convertStreamToString(response: Response<response>): String {
        val reader = BufferedReader(InputStreamReader(response.errorBody()!!.byteStream()))
        val sb = StringBuilder()

        var line = reader.readLine()
        try {
            while (line != null) {
                sb.append(line).append('\n')
                line = reader.readLine()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return sb.toString()
    }
    //endregion

    //region for hide keyboard
    fun hideSoftKeyBoard(context: Context, view: View) {
        try {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            // TODO: handle exception
            e.printStackTrace()
        }
    }
    //endregion
}
